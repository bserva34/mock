package fds.tpMocks;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.when;

import java.time.Clock;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class TestBibliUtilities {
	
	private NoticeBibliographique n1;
	private NoticeBibliographique n2;
	private NoticeBibliographique n3;
	private NoticeBibliographique n4;
	private NoticeBibliographique n5;
	private NoticeBibliographique n6;
	private NoticeBibliographique n7;
	private NoticeBibliographique n8;
	private NoticeBibliographique n9;
	private NoticeBibliographique n10;
	private NoticeBibliographique n11;
	private NoticeBibliographique n12;
	private NoticeBibliographique n13;
	private NoticeBibliographique n14;
	private ArrayList<NoticeBibliographique> listeNotices;
	private ArrayList<NoticeBibliographique> listeRetournee;
	private Bibliothèque b;
	
	@Mock
	Clock mockedClock;
	
	@Mock
	InterfaceBiblio BddExtraiteDeInterface;
	
	@InjectMocks
	BibliUtilities bib = new BibliUtilities();
	
	@BeforeEach
	void setup() {
		n1 = new NoticeBibliographique("1", "Harry Potter à l'école des sorciers", "J.K. Rowling");
		n2 = new NoticeBibliographique("2", "Harry Potter et la chambre des secrets", "J.K. Rowling");
		n3 = new NoticeBibliographique("3", "Harry Potter et le prisonnier d'Azkaban", "J.K. Rowling");
		n4 = new NoticeBibliographique("4", "Harry Potter et la coupe de feu", "J.K. Rowling");
		n5 = new NoticeBibliographique("5", "Harry Potter et l'Ordre du phénix", "J.K. Rowling");
		n6 = new NoticeBibliographique("6", "Harry Potter et le Prince de sang-mélé", "J.K. Rowling");
		n7 = new NoticeBibliographique("7", "Harry Potter et les reliques de la Mort", "J.K. Rowling");
		n8 = new NoticeBibliographique("8", "test8", "Benjamin Serva");	
		n9 = new NoticeBibliographique("9", "test9", "Benjamin Serva");
		n10 = new NoticeBibliographique("10", "test10", "Benjamin Serva");
		n11 = new NoticeBibliographique("11", "test11", "Benjamin Serva");
		n12 = new NoticeBibliographique("12", "test12", "Tom Tisserand");
		n13 = new NoticeBibliographique("13", "test13", "Tom Tisserand");
		n14 = new NoticeBibliographique("14", "test14", "Tom Tisserand");
		
		listeNotices = new ArrayList<NoticeBibliographique>();
		listeRetournee = new ArrayList<NoticeBibliographique>();
		b = Bibliothèque.getInstance();
		b.addNotice(n2);
		b.addNotice(n14);
	}
	/*Test de chercherNoticesConnexes*/
	@Test
	public void testConnexe5TitresOuPlus() {
		listeNotices.add(n1);
		listeNotices.add(n2);
		listeNotices.add(n3);
		listeNotices.add(n4);
		listeNotices.add(n5);
		listeNotices.add(n6);
		listeNotices.add(n7);
		listeRetournee.add(n2);
		listeRetournee.add(n3);
		listeRetournee.add(n4);
		listeRetournee.add(n5);
		listeRetournee.add(n6);
	}
	
	@Test
	public void testConnexe1A4Titres() {
		listeNotices.add(n10);
		listeNotices.add(n11);
		listeNotices.add(n12);
		listeRetournee.add(n11);
		listeRetournee.add(n12);
		
		when(BddExtraiteDeInterface.noticesDuMemeAuteurQue(n10)).thenReturn(listeNotices);
		assertEquals(listeRetournee.size(),bib.chercherNoticesConnexes(n10).size());
	}
	
	@Test
	public void testConnexe0Titre() {
		listeNotices.add(n8);
		when(BddExtraiteDeInterface.noticesDuMemeAuteurQue(n8)).thenReturn(listeNotices);
		assertEquals(listeRetournee.size(),bib.chercherNoticesConnexes(n8).size());
	}
	
	@Test
	public void testConnexeTitreIdentique() {
		listeNotices.add(n13);
		listeNotices.add(n14);
		when(BddExtraiteDeInterface.noticesDuMemeAuteurQue(n13)).thenReturn(listeNotices);
		assertEquals(listeRetournee.size(),bib.chercherNoticesConnexes(n13).size());
	}
	
	/*Test de ajoutNotice*/
	@Test
	public void testAjoutNoticeInexistante() throws IncorrectIsbnException {
		listeNotices.add(n8);
		when(BddExtraiteDeInterface.getNoticeFromIsbn("n20")).thenThrow(IncorrectIsbnException.class);
		assertThrows(AjoutImpossibleException.class, () -> {bib.ajoutNotice("n20");});
	}
	
	@Test
	public void testAjoutNoticeCorrecteNouvelle() throws IncorrectIsbnException, AjoutImpossibleException {
		when(BddExtraiteDeInterface.getNoticeFromIsbn("1")).thenReturn(n1);
		assertEquals(NoticeStatus.newlyAdded,bib.ajoutNotice("1"));
	}
	
	@Test
	public void testAjoutNoticeCorrecteInchangee() throws IncorrectIsbnException, AjoutImpossibleException {
		when(BddExtraiteDeInterface.getNoticeFromIsbn("2")).thenReturn(n2);
		assertEquals(NoticeStatus.nochange,bib.ajoutNotice("2"));
	}
	
	@Test
	public void testAjoutNoticeCorrecteMiseAJour() throws AjoutImpossibleException, IncorrectIsbnException {
		n14 = new NoticeBibliographique("14", "Tour de bocal 2.0", "Tom Tisserand");
		when(BddExtraiteDeInterface.getNoticeFromIsbn("14")).thenReturn(n14);
		assertEquals(NoticeStatus.updated,bib.ajoutNotice("14"));
	}
	
	/*Test de prévoirInventaire*/
	/*nous n'avons pas implementé le lancement effectif de l'inventaire si effectiement le bool est True, car c'est un effect de bord qui ne laisse pas le choix à l'utilisateur (sauf si input, puis tester un input...), il lancera lui meme inventaire() car c'est une methode public de la classe Bibliothèque, la main à l'utilisateur*/
	@Test
	public void testPrévoirInventaire() {
		
		LocalDate uneDate = LocalDate.of(2021, 05, 01);
		Clock fixedClock = Clock.fixed(uneDate.atStartOfDay(ZoneId.systemDefault()).toInstant(), ZoneId.systemDefault());
		doReturn(fixedClock.instant()).when(mockedClock).instant();
		doReturn(fixedClock.getZone()).when(mockedClock).getZone();
		assertEquals(false, bib.prévoirInventaire());
		
		//les deux tests sont dans un seul test parce qu'on manipule une même instance de bibliothèque
		LocalDate autreDate = LocalDate.of(2025, 12, 31);
		fixedClock = Clock.fixed(autreDate.atStartOfDay(ZoneId.systemDefault()).toInstant(), ZoneId.systemDefault());
		doReturn(fixedClock.instant()).when(mockedClock).instant();
		doReturn(fixedClock.getZone()).when(mockedClock).getZone();
		assertEquals(true, bib.prévoirInventaire());
	}
	/*apres avoir update n14, on pourrait faire un rechercheNotice pour voir si y'a bien un titre associé a la liste pour Tom, en utilisant un order, par contre testNotice va faire failure card size renvoyée= 0 et size notice = 1*/
}

